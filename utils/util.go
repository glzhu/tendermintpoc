package utils

import (
	"strconv"
	"time"
)

func GetTimeStamp() string {
	t := time.Now()
	return strconv.FormatInt(t.Unix(), 10)
}
