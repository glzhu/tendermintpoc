/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 	BlockgraphTransaction : the transaction that carry the business logic data & send to blockchain

	ValidationTransaction : the transaction that send from submitter to validation Layer
	(this can be unified by using BlockgraphTransaction)

	TransactionCounter : used by validation layer

=================================*/
package types

import (
	"bytes"
	"strconv"
	// "fmt"
)

type Participant struct {
	Pubkey    []byte
	Name      []byte
	Signature []byte
}

func (p *Participant) ToBytes() []byte {
	var b []byte
	buf := bytes.NewBuffer(b)
	buf.Write(p.Pubkey)
	buf.Write(p.Signature)
	return buf.Bytes()
}

// TxParticipants  sign for {TxId,TxType,TxData}
// Submitter sign for {TxId,TxType,TxData, TxParticipants}
// Validator sign for {TxId,TxType,TxData, TxParticipants, Submitter}
type BlockgraphTransaction struct {
	TxId           []byte
	TxType         int
	TxParticipants []*Participant
	Submitter      *Participant
	Validator      *Participant
	TxData         []byte
}

type ValidationTransaction struct {
	Data           []byte `json:"data"`
	TxParticipants []*Participant
	Submitter      *Participant
}

type TransactionCounter struct {
	Timestamp       int64
	ValidationTxCnt int
	TendermintTxCnt int
}

func (tx *BlockgraphTransaction) TxParticipantSignBytes() []byte {
	var b []byte
	buf := bytes.NewBuffer(b)
	buf.Write(tx.TxId)
	buf.Write([]byte(strconv.Itoa(tx.TxType)))
	buf.Write(tx.TxData)

	return buf.Bytes()
}

func (tx *BlockgraphTransaction) TxSubmitterSignBytes() []byte {
	var b []byte
	buf := bytes.NewBuffer(b)
	buf.Write(tx.TxParticipantSignBytes())

	for _, p := range tx.TxParticipants {
		buf.Write(p.ToBytes())
	}

	return buf.Bytes()
}

func (tx *BlockgraphTransaction) TxValidatorSignBytes() []byte {
	var b []byte
	buf := bytes.NewBuffer(b)
	buf.Write(tx.TxSubmitterSignBytes())
	buf.Write(tx.Submitter.ToBytes())

	return buf.Bytes()
}

func (tx *BlockgraphTransaction) ToBytes() []byte {
	var b []byte
	buf := bytes.NewBuffer(b)
	buf.Write(tx.TxSubmitterSignBytes())
	buf.Write(tx.Submitter.ToBytes())

	return buf.Bytes()
}

//Mem cache : used in loading reputation periodically
type ReputationMemCache struct {
	ReputationMap map[string]int
}
