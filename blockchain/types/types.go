package types

import (
	blockgraph "tendermintpoc/blockgraph/types"
)

const (
	TYPE_NODE_VOTER = iota
	TYPE_NODE_OBSERVER
	TYPE_NODE_DOWN
)

// Define the method to manager the nodes
type BlockchainNodeManager interface {
	NodeSetVotingPower(pub string, power int64) error
	NodeRemove(pub string) error
	NodeSetToObserver(pub string) error
	NodeSetToVoter(pub string) error
	NodeGetType(pub string) (int, error)
}

// Define the method to query world-states
// For check business status
type BlockchainStateQueryManager interface {
	StateQuery(key string) (interface{}, error)
}

// Define the interface to present a block 
type BlockInfo interface {
	ToString() string
}

// Define the interface to query a block
//For audit, validation of history business transactions
type BlockchainBlockQueryManager interface {
	BlockQuery(height int64) (BlockInfo, error)
}

// Define the interface of transaction tasks (submit, validate, recall)
type BlockchainTransactionManager interface {
	TransactionSubmit(tx *blockgraph.BlockgraphTransaction) error
}

//for embed
type BlockchainSdk struct {
	NodeManager        BlockchainNodeManager
	StateQueryManager  BlockchainStateQueryManager
	BlockQueryManager  BlockchainBlockQueryManager
	TransactionManager BlockchainTransactionManager
}

func (this *BlockchainSdk) GetBlockchainNodeManager() BlockchainNodeManager {
	return this.NodeManager
}

func (this *BlockchainSdk) GetBlockchainStateQueryManager() BlockchainStateQueryManager {
	return this.StateQueryManager
}

func (this *BlockchainSdk) GetBlockchainBlockQueryManager() BlockchainBlockQueryManager {
	return this.BlockQueryManager
}

func (this *BlockchainSdk) GetBlockchainTransactionManager() BlockchainTransactionManager {
	return this.TransactionManager
}
