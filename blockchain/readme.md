# About this package
contains the code about blockchain plugin and the implementation of different blockchian product


# How to add a plugin

*1* Create a folder under blockchain, named as "yourblockchain_plugin" 
*2* Creat a file called "yourblockchain_plugin.go"

*3* Create your own plugin struct
```
type YourBlockchainPlugin struct {
    blockchain.BlockchainSdk   //make sure you embed this Sdk
}

```

Here is the model
```
type BlockchainSdk struct {
    NodeManager        BlockchainNodeManager
    StateQueryManager  BlockchainStateQueryManager
    BlockQueryManager  BlockchainBlockQueryManager
    TransactionManager BlockchainTransactionManager
} 
```

Implement the Managers Interfaces you want to use
* BlockchainNodeManager : for check,add,remove nodes, set voting power
* BlockchainStateQueryManager : for query state
* BlockchainBlockQueryManager : for query block history
* BlockchainTransactionManager : for submt transaction

```
var _ blockchain.BlockchainStateQueryManager = (*QuorumStateQueryManager)(nil)

type QuorumStateQueryManager struct {
}

func (this *QuorumStateQueryManager) StateQuery(key string) (interface{}, error) {
    ...
    here to really query your blockchain
    ...
}
```

And then use your manager to instantiate your plugin
```
func NewQuorumPlugin(config *QuorumPluginConfig) *QuorumPlugin {
    plugin := &QuorumPlugin{}
    plugin.NodeManager = &QuorumNodeManager{url: config.BaseUrl + config.SubmitTxUrl}
    plugin.StateQueryManager = &QuorumStateQueryManager{}
    plugin.BlockQueryManager = &QuorumBlockQueryManager{}
    plugin.TransactionManager = &QuorumTransactionManager{}

    return plugin
}

```

*4* Add your plugin to factory
Add  InitYoutBlockchainPlugin()
```
config := &quorum_plugin.QuorumPluginConfig{
        BaseUrl:       nodeAddr,
        SubmitTxUrl:   "",
        QueryTxUrl:    "",
        QueryBlockUrl: "",
    }
quorumPlugin = quorum_plugin.NewQuorumPlugin(config)
```

At last change SetPlugin()
```
func SetPlugin(pluginType int) {
    switch pluginType {
    case PLUGIN_TENDERMINT:
        selectedPlugin = tendermintPlugin
    case PLUGIN_QUORUM:
        selectedPlugin = quorumPlugin
    case YourBlockchainPlugin:
        selectedPlugin = yourPlugin
    }
}
```


