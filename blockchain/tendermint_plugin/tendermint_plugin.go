package tendermint_plugin

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	blockchain "tendermintpoc/blockchain/types"
	blockgraph "tendermintpoc/blockgraph/types"
)

var (
	NotImplErr = errors.New("Not Implemented")
)

type TendermintPlugin struct {
	blockchain.BlockchainSdk
}

type TendermintPluginConfig struct {
	BaseUrl       string
	SubmitTxUrl   string
	QueryTxUrl    string
	QueryBlockUrl string
}

func NewTendermintPlugin(config *TendermintPluginConfig) *TendermintPlugin {
	plugin := &TendermintPlugin{}
	plugin.NodeManager = &TendermintNodeManager{baseUrl: config.BaseUrl}
	plugin.StateQueryManager = &TendermintStateQueryManager{}
	plugin.BlockQueryManager = &TendermintBlockQueryManager{}
	plugin.TransactionManager = &TendermintTransactionManager{postUrl: config.BaseUrl}

	return plugin
}

//----------------
var _ blockchain.BlockchainNodeManager = (*TendermintNodeManager)(nil)

type TendermintNodeManager struct {
	baseUrl  string
	powerMap map[string]int64
}

func (this *TendermintNodeManager) NodeSetVotingPower(pub string, power int64) error {
	fullurl := this.baseUrl + "/broadcast_tx_commit?tx=" + "\"val:01" + pub + "/" + strconv.Itoa(int(power)) + "\""
	log.Print("Set Request " + fullurl)
	resp, err := http.Get(fullurl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(string(body))

	if this.powerMap == nil {
		this.powerMap = make(map[string]int64)
	}
	this.powerMap[pub] = power
	return nil
	// return NotImplErr
}

func (this *TendermintNodeManager) NodeRemove(pub string) error {
	return this.NodeSetVotingPower(pub, 0)
}

func (this *TendermintNodeManager) NodeSetToObserver(pub string) error {
	return this.NodeSetVotingPower(pub, 0)
}

func (this *TendermintNodeManager) NodeSetToVoter(pub string) error {
	return this.NodeSetVotingPower(pub, 50)
}

func (this *TendermintNodeManager) NodeGetType(pub string) (int, error) {
	if this.powerMap == nil {
		return -1, errors.New("Node Information Empty")
	}
	return int(this.powerMap[pub]), nil
}

//---------------
var _ blockchain.BlockchainStateQueryManager = (*TendermintStateQueryManager)(nil)

type TendermintStateQueryManager struct {
}

func (this *TendermintStateQueryManager) StateQuery(key string) (interface{}, error) {
	return nil, NotImplErr
}

//---------------
var _ blockchain.BlockchainBlockQueryManager = (*TendermintBlockQueryManager)(nil)

type TendermintBlockInfo struct {
}

func (this *TendermintBlockInfo) ToString() string {
	return ""
}

type TendermintBlockQueryManager struct {
}

func (this *TendermintBlockQueryManager) BlockQuery(height int64) (blockchain.BlockInfo, error) {
	return &TendermintBlockInfo{}, NotImplErr
}

//---------------
var _ blockchain.BlockchainTransactionManager = (*TendermintTransactionManager)(nil)

type TendermintTransactionManager struct {
	postUrl string
}

type TendermintPostInnerTx struct {
	Tx string `json:"tx"`
}

type TendermintPostTx struct {
	Jsonrpc string                 `json:"jsonrpc"`
	Id      string                 `json:"id"`
	Method  string                 `json:"method"`
	Params  *TendermintPostInnerTx `json:"params"`
}

func (this *TendermintTransactionManager) TransactionSubmit(tx *blockgraph.BlockgraphTransaction) error {
	//encode blockgraph transaction
	postTx := &TendermintPostTx{
		Jsonrpc: "2.0",
		Id:      string(tx.TxId),
		Method:  "broadcast_tx_commit",
	}

	jsonValue, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	// fmt.Println(string(jsonValue))
	encoded := base64.StdEncoding.EncodeToString(jsonValue)
	// fmt.Println("encoded to base64: " + string(encoded))

	postTx.Params = &TendermintPostInnerTx{Tx: string(encoded)}
	// "{\"tx\" : \"" + string(encoded) + "\"}"

	postUrl := this.postUrl + ""
	postTxJsonValue, err := json.Marshal(postTx)
	if err != nil {
		return err
	}
	fmt.Printf("Send post url to Tendermint:%s , body:%s \n", postUrl, string(postTxJsonValue))

	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(postTxJsonValue))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("Response Status from Tendermint:", resp.Status)
	// fmt.Println("response Headers:", resp.Header)
	// body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println("response Body:", string(body))
	return nil
}
