package tendermint_plugin

import (
	"errors"
	// "net/http"
	blockchain "tendermintpoc/blockchain/types"
	blockgraph "tendermintpoc/blockgraph/types"
)

var (
	NotImplErr = errors.New("Not Implemented")
)

//must be defined in this way
type QuorumPlugin struct {
	blockchain.BlockchainSdk
}

//custom your own config
type QuorumPluginConfig struct {
	BaseUrl       string
	SubmitTxUrl   string
	QueryTxUrl    string
	QueryBlockUrl string
}

func NewQuorumPlugin(config *QuorumPluginConfig) *QuorumPlugin {
	plugin := &QuorumPlugin{}
	plugin.NodeManager = &QuorumNodeManager{url: config.BaseUrl + config.SubmitTxUrl}
	plugin.StateQueryManager = &QuorumStateQueryManager{}
	plugin.BlockQueryManager = &QuorumBlockQueryManager{}
	plugin.TransactionManager = &QuorumTransactionManager{}

	return plugin
}

//----------------
var _ blockchain.BlockchainNodeManager = (*QuorumNodeManager)(nil)

type QuorumNodeManager struct {
	url string
}

func (this *QuorumNodeManager) NodeSetVotingPower(pub string, power int64) error {
	return NotImplErr
}

func (this *QuorumNodeManager) NodeRemove(pub string) error {
	return NotImplErr
}

func (this *QuorumNodeManager) NodeSetToObserver(pub string) error {
	return NotImplErr
}

func (this *QuorumNodeManager) NodeSetToVoter(pub string) error {
	return NotImplErr
}

func (this *QuorumNodeManager) NodeGetType(pub string) (int, error) {
	return -1, NotImplErr
}

//---------------
var _ blockchain.BlockchainStateQueryManager = (*QuorumStateQueryManager)(nil)

type QuorumStateQueryManager struct {
}

func (this *QuorumStateQueryManager) StateQuery(key string) (interface{}, error) {
	return nil, NotImplErr
}

//---------------
var _ blockchain.BlockchainBlockQueryManager = (*QuorumBlockQueryManager)(nil)

type QuorumBlockInfo struct {
}

func (this *QuorumBlockInfo) ToString() string {
	return ""
}

type QuorumBlockQueryManager struct {
}

func (this *QuorumBlockQueryManager) BlockQuery(height int64) (blockchain.BlockInfo, error) {
	return &QuorumBlockInfo{}, NotImplErr
}

//---------------
var _ blockchain.BlockchainTransactionManager = (*QuorumTransactionManager)(nil)

type QuorumTransactionManager struct {
}

func (this *QuorumTransactionManager) TransactionSubmit(tx *blockgraph.BlockgraphTransaction) error {
	return NotImplErr
}
