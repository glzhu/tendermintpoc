/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 test use PLUGIN_TENDERMINT to set voting power
 must start the cluster before and then replace the pubkey here and run
=================================*/
package main

import (
	"fmt"
	blockchain "tendermintpoc/blockchain"
	// transaction "tendermintpoc/blockgraph/types"
)

var blockchianPlugin blockchain.BlockchainPlugin

func InitBlockchain(url string) {
	blockchain.InitTendermintPlugin(url)
	// blockchain.InitQuorumPlugin(nodeAddress)

	blockchain.SetPlugin(blockchain.PLUGIN_TENDERMINT)

	blockchianPlugin = blockchain.GetBlockchainPlugin()
}

func main() {
	//read your config
	nodeAddress := "http://localhost:46667"

	InitBlockchain(nodeAddress)

	//use the blockchain
	err := blockchianPlugin.GetBlockchainNodeManager().NodeSetVotingPower("2557565DEC4043BFB739248673A87044B3559D8A515F76ED23EE56058FCA60E9", 30)
	if err != nil {
		fmt.Printf("Submit Tx Err %s \n", err)
	}
}
