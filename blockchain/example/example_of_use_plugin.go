/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 test use PLUGIN_TENDERMINT to submit transaction
 must start the cluster before and then replace the pubkey here and run
=================================*/
package main

import (
	"fmt"
	blockchain "tendermintpoc/blockchain"
	transaction "tendermintpoc/blockgraph/types"
)

var blockchianPlugin blockchain.BlockchainPlugin

func InitBlockchain(url string) {
	blockchain.InitTendermintPlugin(url)
	// blockchain.InitQuorumPlugin(nodeAddress)

	blockchain.SetPlugin(blockchain.PLUGIN_TENDERMINT)

	blockchianPlugin = blockchain.GetBlockchainPlugin()
}

func main() {
	//read your config
	nodeAddress := "http://localhost:46667"

	InitBlockchain(nodeAddress)

	//fake a transaction

	bgTx := &transaction.BlockgraphTransaction{
		TxId:           []byte("123456789"),
		TxType:         100,
		TxData:         []byte("qwertyuio"),
		TxParticipants: []*transaction.Participant{},
		Submitter:      nil,
	}

	//use the blockchain
	err := blockchianPlugin.GetBlockchainTransactionManager().TransactionSubmit(bgTx)
	if err != nil {
		fmt.Printf("Submit Tx Err %s \n", err)
	}
}
