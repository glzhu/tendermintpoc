/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :

	How to use the factory :
	1 InitTendermintPlugin/ Or other
	2 SetPlugin   when you are doing the init jobs for your services
	3 GetBlockchainPlugin  at where you really need to interact with blockchain


=================================*/

package blockchain

import (
	"fmt"
	quorum_plugin "tendermintpoc/blockchain/quorum_plugin"
	tendermint_plugin "tendermintpoc/blockchain/tendermint_plugin"

	types "tendermintpoc/blockchain/types"
)

var (
	tendermintPlugin *tendermint_plugin.TendermintPlugin
	quorumPlugin     *quorum_plugin.QuorumPlugin

	selectedPlugin BlockchainPlugin
)

const (
	PLUGIN_TENDERMINT = 1
	PLUGIN_QUORUM     = 2
)

var _ BlockchainPlugin = (*tendermint_plugin.TendermintPlugin)(nil)
var _ BlockchainPlugin = (*quorum_plugin.QuorumPlugin)(nil)

type BlockchainPlugin interface {
	GetBlockchainNodeManager() types.BlockchainNodeManager
	GetBlockchainStateQueryManager() types.BlockchainStateQueryManager
	GetBlockchainBlockQueryManager() types.BlockchainBlockQueryManager
	GetBlockchainTransactionManager() types.BlockchainTransactionManager
}

func InitTendermintPlugin(nodeAddr string) {
	//choose which plugin to be used here
	//may do that by read config
	fmt.Println("Tendermint as the blockchain plugin")

	config := &tendermint_plugin.TendermintPluginConfig{
		BaseUrl:       nodeAddr,
		SubmitTxUrl:   "",
		QueryTxUrl:    "",
		QueryBlockUrl: "",
	}
	tendermintPlugin = tendermint_plugin.NewTendermintPlugin(config)
}

func InitQuorumPlugin(nodeAddr string) {
	//choose which plugin to be used here
	//may do that by read config
	fmt.Println("Quorum as the blockchain plugin")

	config := &quorum_plugin.QuorumPluginConfig{
		BaseUrl:       nodeAddr,
		SubmitTxUrl:   "",
		QueryTxUrl:    "",
		QueryBlockUrl: "",
	}
	quorumPlugin = quorum_plugin.NewQuorumPlugin(config)
}

func SetPlugin(pluginType int) {
	switch pluginType {
	case PLUGIN_TENDERMINT:
		selectedPlugin = tendermintPlugin
	case PLUGIN_QUORUM:
		selectedPlugin = quorumPlugin
	}
}

func GetBlockchainPlugin() BlockchainPlugin {
	return selectedPlugin
}
