/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 This file is for  testing use the tendermint's crypto package
=================================*/

package main

import (
	"encoding/hex"
	"fmt"
	abci "github.com/tendermint/abci/types"
	crypto "github.com/tendermint/go-crypto"
	// "github.com/tendermint/tendermint/proxy"
	// "github.com/tendermint/tendermint/types"
	// dbm "github.com/tendermint/tmlibs/db"
	// "github.com/tendermint/tmlibs/log"
)

func main() {

	origin := "01C570ABE536F00C6789CAD4DD948C589DAA78F78ADD1167AF7A6DF6248D4A6809"

	pubkey, err := hex.DecodeString(origin)
	if err != nil {
		fmt.Println("hex decode error " + err.Error())
		return
	}
	power := 44

	v := abci.Validator{
		pubkey, int64(power)}

	{
		pubkey, err := crypto.PubKeyFromBytes(v.PubKey) // NOTE: expects go-wire encoded pubkey
		if err != nil {
			fmt.Println("crypto decode error " + err.Error())
			return
		}
		address := pubkey.Address()
		power := int64(v.Power)

		fmt.Printf("%v\n", address)
		fmt.Printf("%x\n", (pubkey.Bytes()))

		if power < 0 {
			fmt.Printf("Power (%d) overflows int64 \n", v.Power)
		}
	}

}
