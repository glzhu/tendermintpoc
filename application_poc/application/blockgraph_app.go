/*=================================
Author: glzhu
Generate Date: 2018-04-12
Inc: Freewheel
Description :
 This file is for
=================================*/

package blockgraph

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"

	"github.com/tendermint/abci/example/code"
	"github.com/tendermint/abci/types"
	// crypto "github.com/tendermint/go-crypto"
	cmn "github.com/tendermint/tmlibs/common"
	dbm "github.com/tendermint/tmlibs/db"
	"github.com/tendermint/tmlibs/log"
)

//The tx format should be val:pubkey/power/timestamp

const (
	ValidatorSetChangePrefix string = "val:"
)

//-----------------------------------------

var _ types.Application = (*BlockGraphApplication)(nil)

type BlockGraphApplication struct {
	app *KVStoreApplication

	// validator set
	ValUpdates []types.Validator

	logger log.Logger
}

func NewBlockGraphApplication(dbDir string) *BlockGraphApplication {
	name := "kvstore"
	db, err := dbm.NewGoLevelDB(name, dbDir)
	if err != nil {
		panic(err)
	}

	state := loadState(db)

	return &BlockGraphApplication{
		app:    &KVStoreApplication{state: state},
		logger: log.NewNopLogger(),
	}
}

func (app *BlockGraphApplication) SetLogger(l log.Logger) {
	app.logger = l
}

func (app *BlockGraphApplication) Info(req types.RequestInfo) types.ResponseInfo {
	res := app.app.Info(req)
	res.LastBlockHeight = app.app.state.Height
	res.LastBlockAppHash = app.app.state.AppHash
	return res
}

func (app *BlockGraphApplication) SetOption(req types.RequestSetOption) types.ResponseSetOption {
	return app.app.SetOption(req)
}

// Here is the entrance for process the voting power change TX
// tx is either "val:pubkey/power" or "key=value" or just arbitrary bytes
func (app *BlockGraphApplication) DeliverTx(tx []byte) types.ResponseDeliverTx {
	// if it starts with "val:", update the validator set
	// format is "val:pubkey/power"
	if isValidatorTx(tx) {
		// update validators in the merkle tree
		// and in app.ValUpdates
		fmt.Printf("a1 new validator Tx : %s", string(tx))
		return app.execValidatorTx(tx)
	}

	// otherwise, update the key-value store
	return app.app.DeliverTx(tx)
}

func (app *BlockGraphApplication) CheckTx(tx []byte) types.ResponseCheckTx {
	return app.app.CheckTx(tx)
}

// Commit will panic if InitChain was not called
func (app *BlockGraphApplication) Commit() types.ResponseCommit {
	return app.app.Commit()
}

/*
* Two Query Types
* 1:  normal KV
* 2:  votingPower KV : start with `val:`
 */
func (app *BlockGraphApplication) Query(reqQuery types.RequestQuery) (resQuery types.ResponseQuery) {
	if reqQuery.Prove {

		// process the voting power transaction
		if strings.HasPrefix(string(reqQuery.Data), ValidatorSetChangePrefix) {
			// fmt.Printf("Blockgraph Query power : %s\n", string(reqQuery.Data))
			hpubkey := reqQuery.Data[len(ValidatorSetChangePrefix):]

			//must be go-crypted or the tendermint-core will panic
			pubkey, err := hex.DecodeString(string(hpubkey))
			if err != nil {
				resQuery.Index = -1 // TODO make Proof return index
				resQuery.Key = reqQuery.Data
				resQuery.Value = nil
				resQuery.Log = fmt.Sprintf("Pubkey (%s) is invalid hex", string(hpubkey))
				return
			}

			dbkey := []byte(ValidatorSetChangePrefix + string(pubkey))

			if app.app.state.db.Has(dbkey) {
				value := app.app.state.db.Get(dbkey)
				resQuery.Index = -1 // TODO make Proof return index
				resQuery.Key = reqQuery.Data
				if value != nil {
					resQuery.Log = "exists"
				} else {
					resQuery.Log = "does not exist"
				}

				buffer := bytes.NewReader(value)
				v := types.Validator{}
				readErr := types.ReadMessage(buffer, &v)
				if readErr != nil {
					resQuery.Value = []byte(readErr.Error())
					resQuery.Log = "exists , but read error"
				} else {
					resQuery.Value = []byte(strconv.Itoa(int(v.Power)))
				}
				return
			}
		}
	}
	return app.app.Query(reqQuery)
}

// Save the validators in the merkle tree
func (app *BlockGraphApplication) InitChain(req types.RequestInitChain) types.ResponseInitChain {
	app.logger.Info("-------- Init BlockGraph!! ---------")
	for _, v := range req.Validators {
		r := app.updateValidator(v)
		if r.IsErr() {
			app.logger.Error("Error updating validators", "r", r)
		}
	}
	return types.ResponseInitChain{}
}

// Track the block hash and header information
func (app *BlockGraphApplication) BeginBlock(req types.RequestBeginBlock) types.ResponseBeginBlock {
	// reset valset changes
	app.ValUpdates = make([]types.Validator, 0)
	return types.ResponseBeginBlock{}
}

// Update the validator set
func (app *BlockGraphApplication) EndBlock(req types.RequestEndBlock) types.ResponseEndBlock {
	return types.ResponseEndBlock{ValidatorUpdates: app.ValUpdates}
}

//---------------------------------------------
// update validators

func (app *BlockGraphApplication) Validators() (validators []types.Validator) {
	itr := app.app.state.db.Iterator(nil, nil)
	for ; itr.Valid(); itr.Next() {
		if isValidatorTx(itr.Key()) {
			validator := new(types.Validator)
			err := types.ReadMessage(bytes.NewBuffer(itr.Value()), validator)
			if err != nil {
				panic(err)
			}
			validators = append(validators, *validator)
		}
	}
	return
}

func MakeValSetChangeTx(pubkey []byte, power int64) []byte {
	return []byte(cmn.Fmt("val:%X/%d", pubkey, power))
}

func isValidatorTx(tx []byte) bool {
	return strings.HasPrefix(string(tx), ValidatorSetChangePrefix)
}

// format is "val:pubkey1/power1,addr2/power2,addr3/power3"tx
func (app *BlockGraphApplication) execValidatorTx(tx []byte) types.ResponseDeliverTx {
	tx = tx[len(ValidatorSetChangePrefix):]

	//get the pubkey and power
	pubKeyAndPower := strings.Split(string(tx), "/")
	if len(pubKeyAndPower) != 2 {
		return types.ResponseDeliverTx{
			Code: code.CodeTypeEncodingError,
			Log:  fmt.Sprintf("Expected 'pubkey/power'. Got %v", pubKeyAndPower)}
	}
	pubkeyS, powerS := pubKeyAndPower[0], pubKeyAndPower[1]

	//must be go-crypted or the tendermint-core will panic
	pubkey, err := hex.DecodeString(pubkeyS)
	if err != nil {
		return types.ResponseDeliverTx{
			Code: code.CodeTypeEncodingError,
			Log:  fmt.Sprintf("Pubkey (%s) is invalid hex", pubkeyS)}
	}
	// pubkey := []byte(pubkeyS)

	// _, err = crypto.PubKeyFromBytes(pubkey)
	// if err != nil {
	// 	return types.ResponseDeliverTx{
	// 		Code: code.CodeTypeEncodingError,
	// 		Log:  fmt.Sprintf("Pubkey (%X) is invalid go-crypto encoded", pubkey)}
	// }

	// decode the power
	power, err := strconv.ParseInt(powerS, 10, 64)
	if err != nil {
		return types.ResponseDeliverTx{
			Code: code.CodeTypeEncodingError,
			Log:  fmt.Sprintf("Power (%s) is not an int", powerS)}
	}

	fmt.Printf("Handle ValidatorTx types.Validator{%s, %d}", pubkey, power)
	// update , pubkey here must be 32 bytes
	return app.updateValidator(types.Validator{pubkey, power})
}

// add, update, or remove a validator
func (app *BlockGraphApplication) updateValidator(v types.Validator) types.ResponseDeliverTx {
	key := []byte("val:" + string(v.PubKey))
	fmt.Printf("update val : %s\n", string(key))

	if v.Power == 0 {
		// remove validator
		if !app.app.state.db.Has(key) {
			return types.ResponseDeliverTx{
				Code: code.CodeTypeUnauthorized,
				Log:  fmt.Sprintf("Cannot remove non-existent validator %X", key)}
		}
		app.app.state.db.Delete(key)
	} else {
		// add or update validator
		value := bytes.NewBuffer(make([]byte, 0))
		if err := types.WriteMessage(&v, value); err != nil {
			return types.ResponseDeliverTx{
				Code: code.CodeTypeEncodingError,
				Log:  fmt.Sprintf("Error encoding validator: %v", err)}
		}
		// value := v.Power
		// app.app.state.db.Set(key, []byte(strconv.Itoa(int(value))))
		app.app.state.db.Set(key, value.Bytes())
	}

	// we only update the changes array if we successfully updated the tree
	app.ValUpdates = append(app.ValUpdates, v)

	return types.ResponseDeliverTx{Code: code.CodeTypeOK}
}
