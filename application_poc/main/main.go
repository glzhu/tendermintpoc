/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 here is a simple server , run on localhost:8888
 that means now tendermint core and this server-binary are run on the same server.
 In the config of tendermint deployment, give tendermint core `127.0.0.1:8888` as the ABCI application address
=================================*/

package main

import (
	"fmt"
	abciserver "github.com/tendermint/abci/server"
	"github.com/tendermint/tmlibs/log"
	"tendermintpoc/application_poc/blockgraph"
	// abcicli "github.com/tendermint/abci/client"
	"os"
	"time"
)

const (
	DIR = "/tmp/blockgraphDB/"
)

func main() {
	app := blockgraph.NewBlockGraphApplication(DIR)
	server := abciserver.NewSocketServer("127.0.0.1:8888", app)

	logFile, logErr := os.Create(DIR + "abciserver_" + time.Now().Format("2006-01-02 15:04:05") + ".log")
	if logErr != nil {
		fmt.Println("fail to create log file")
		return
	}

	logger := log.NewTMLogger(logFile).With("module", "abci-server")

	server.SetLogger(logger)

	if err := server.Start(); err != nil {
		logger.Error(fmt.Sprintf("Error starting socket server: %v", err.Error()))
	}

	<-server.Quit() //block until the server is quit
}
