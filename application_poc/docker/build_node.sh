#!/bin/sh
echo "1 building abciServer ..." 
cd $GOPATH/src/
docker run -v $GOPATH/src/:/go/src -w "/go/src/tendermintpoc/application_poc/main" golang go build -o abciServer

echo "success -> `ls $GOPATH/src/tendermintpoc/application_poc/main`"

cd $GOPATH/src/tendermintpoc/application_poc

echo "2 building image blockgraph/tendermint" 

docker build -t blockgraph/tendermint -f docker/Dockerfile .

echo "3 success, remove abciBinaryLocal" 

rm ./main/abciServer

#docker login --username=blockgraph` 
#password is `blockgraph0123`
#docker push blockgraph/tendermint:latest
