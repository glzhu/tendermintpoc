package prometheus_reader

import (
	// "reflect"
	"fmt"
	"testing"
)

func TestPrometheusReader_Query(t *testing.T) {
	type fields struct {
		Url string
	}
	type args struct {
		query string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *PrometheusResponse
		wantErr bool
	}{
		{
			name:   "test1",
			fields: fields{Url: "http://localhost:9090"},
			args:   args{query: "up"},
			want: &PrometheusResponse{
				Status: "success",
				Data:   &PrometheusResponseData{ResultType: "matrix"},
			},
			wantErr: false,
		},
		{
			name:   "test2",
			fields: fields{Url: "http://localhost:19090"},
			args:   args{query: "up"},
			want: &PrometheusResponse{
				Status: "success",
				Data:   &PrometheusResponseData{ResultType: "matrix"},
			},
			wantErr: true,
		},
		{
			name:   "test3",
			fields: fields{Url: "http://localhost:9090"},
			args:   args{query: "uup=22"},
			want: &PrometheusResponse{
				Status: "success",
				Data:   &PrometheusResponseData{ResultType: "matrix"},
			},
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &PrometheusReader{
				Url: tt.fields.Url,
			}
			got, err := r.Query(tt.args.query)
			fmt.Printf("%v %v \n", got, err)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrometheusReader.Query() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil {
				return
			}

			if got.Status != tt.want.Status {
				t.Errorf("PrometheusReader.Query() = %v, want %v", got, tt.want)
			}

			fmt.Printf("%s\n", string(got.Data.Result))

		})
	}
}
