package prometheus_reader

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type PrometheusReader struct {
	Url string //addr + port
}

func NewPrometheusReader(url string) *PrometheusReader {
	reader := &PrometheusReader{Url: url}
	return reader
}

type PrometheusResponse struct {
	Data   *PrometheusResponseData `json:data`
	Status string                  `json:status`
}

type PrometheusResponseData struct {
	ResultType string          `json:resultType`
	Result     json.RawMessage `json:result`
}

type PrometheusResults []*PrometheusResultItem

type PrometheusResultItem struct {
	Metric *PrometheusResultMetric `json:metric`
	Value  json.RawMessage         `json:value`
	Values []json.RawMessage       `json:values`
}

type PrometheusResultMetric struct {
	__name__ string
	Id       string `json:id`
	Image    string
	Instance string
	Job      string
	Name     string `json:name`
}

// URL query parameters:
// query=<string>: Prometheus expression query string.
// time=<rfc3339 | unix_timestamp>: Evaluation timestamp. Optional.
// timeout=<duration>: Evaluation timeout. Optional. Defaults to and is capped by the value of the -query.timeout flag.

func (r *PrometheusReader) Query(query string) (*PrometheusResponse, error) {
	url := r.Url + "/api/v1/query?query=" + query
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Http Get Request Error: " + err.Error())
		return nil, err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println("body,", string(body))

	respObj := &PrometheusResponse{}

	err = json.Unmarshal(body, &respObj)
	if err != nil {
		fmt.Println("Raw response Unmarshal Error: " + err.Error())
		return nil, err
	}

	if respObj.Status != "success" {
		fmt.Println("Response status is not success")
		fmt.Println(string(body))
		return nil, errors.New("Response status is not success")
	}

	return respObj, nil
}

type ValueArray []interface{}

func (r *PrometheusReader) QueryNameAndValue(query string) (map[string]interface{}, error) {
	retMap := make(map[string]interface{}, 10)

	resp, err := r.Query(query)
	if err != nil {
		fmt.Println(err.Error())
		return retMap, err
	}

	if resp.Data.ResultType == "vector" {
		results := PrometheusResults{}
		// fmt.Println("resp.Data.Result", string(resp.Data.Result))
		if err := json.Unmarshal(resp.Data.Result, &results); err != nil {
			fmt.Println(err.Error())
			return retMap, err
		}

		if len(results) <= 0 {
			fmt.Println("empty results")
			return retMap, errors.New("empty results")
		}

		for _, v := range results {
			if len(v.Value) > 0 {
				array := ValueArray{}
				if err := json.Unmarshal(v.Value, &array); err != nil {
					fmt.Println(err.Error())
					return retMap, err
				}
				retMap[v.Metric.Name] = array[1] //0 is timestamp, 1 is query value
			}
		}
	}
	return retMap, nil
}
