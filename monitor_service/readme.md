# About this package
the monitor service

# Code structure
monitor_deployment_docker : 
    prometheus + cadvisor + grafana : configs and dockerfiles.

    Now the demo worked on docker-compose (I create a docker network in script and the all compoment is in the same network), you can also run it on swarm mode and then custom your on network.

prometheus_reader : 
    A client for you to easily got metrics from prometheus 
    (used by reputation calculator)

    You can query prometheus by your self through HTTP API. See the Doc.

# k8s :
    1 run prometheus on k8s
    2 use `deamon Set` to run node-exporter on each k8s node
    3 use prometheus to monitor k8s cluster
    4 add /metrics dispatch for your business services and add the address to prometheus


# Challenge : 
    Prometheus is a single binary. Current services is not in High Availablity. You need do it yourself 