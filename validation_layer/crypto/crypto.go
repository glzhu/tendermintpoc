/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 Provide the Sign/unsign interfaces

	for tendermint using Ed25519,
	I provide both Ed25519 and Rsa algorithm


=================================*/

package crypto

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	// "crypto/x509"
	// "encoding/base64"
	"encoding/hex"
	// "encoding/pem"
	"errors"
	// "fmt"
	ed25519 "golang.org/x/crypto/ed25519"
)

//Sign/unsign interfaces
type PublicKey interface {
	Verify(message []byte, sig []byte) error
}
type PrivateKey interface {
	Sign(data []byte) ([]byte, error)
}

//Ed25519  ---------------------------
type Ed25519PublicKey struct {
	ed25519.PublicKey
}

type Ed25519PrivateKey struct {
	ed25519.PrivateKey
}

func GetPrivateKeyEd25519(key []byte) ed25519.PrivateKey {
	hexBytes := key
	decodedHex, _ := hex.DecodeString(string(hexBytes))
	return ed25519.PrivateKey(decodedHex)
}

func GetPublicKeyEd25519(key []byte) ed25519.PublicKey {
	hexBytes := key
	decodedHex, _ := hex.DecodeString(string(hexBytes))
	return ed25519.PublicKey(decodedHex)
}

func (r *Ed25519PrivateKey) Sign(data []byte) ([]byte, error) {
	sig := ed25519.Sign(r.PrivateKey, data)
	return sig, nil
}

func (r *Ed25519PublicKey) Verify(message []byte, sig []byte) error {
	if !ed25519.Verify(r.PublicKey, message, sig) {
		return errors.New("Signature verify failed")
	}
	return nil
}

//Rsa ----------------------------

type rsaPublicKey struct {
	*rsa.PublicKey
}

type rsaPrivateKey struct {
	*rsa.PrivateKey
}

// Sign signs data with rsa-sha256
func (r *rsaPrivateKey) Sign(data []byte) ([]byte, error) {
	h := sha256.New()
	h.Write(data)
	d := h.Sum(nil)
	return rsa.SignPKCS1v15(rand.Reader, r.PrivateKey, crypto.SHA256, d)
}

// Unsign verifies the message using a rsa-sha256 signature
func (r *rsaPublicKey) Verify(message []byte, sig []byte) error {
	h := sha256.New()
	h.Write(message)
	d := h.Sum(nil)
	return rsa.VerifyPKCS1v15(r.PublicKey, crypto.SHA256, d, sig)
}
