package main

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	ed25519 "golang.org/x/crypto/ed25519"
)

//		 pub: B3CB528297BE25E1126AAF3EE58EFDB1C2BBEB3537E5030E70C24E72F6C9D9DD
//generated : 2e77b18632b4f9caf8f2217979bac5c6b40efc1417406a71ad1b7a02a81e1e44

//pri:        6075D2AECA01C5FE0245A4C35F3616F4179010A2FEFC0D7C6BFB4AA4B91D2407B3CB528297BE25E1126AAF3EE58EFDB1C2BBEB3537E5030E70C24E72F6C9D9DD
//generated : b356506b0d531b5dc99a0c6ec933a3b7aa17667d1d539868ae24dceea60a1b362e77b18632b4f9caf8f2217979bac5c6b40efc1417406a71ad1b7a02a81e1e44
func GetPrivateKey() ed25519.PrivateKey {
	hexBytes := []byte("6075D2AECA01C5FE0245A4C35F3616F4179010A2FEFC0D7C6BFB4AA4B91D2407B3CB528297BE25E1126AAF3EE58EFDB1C2BBEB3537E5030E70C24E72F6C9D9DD")
	decodedHex, _ := hex.DecodeString(string(hexBytes))
	return ed25519.PrivateKey(decodedHex)
}

func GetPublicKey() ed25519.PublicKey {
	hexBytes := []byte("B3CB528297BE25E1126AAF3EE58EFDB1C2BBEB3537E5030E70C24E72F6C9D9DD")
	decodedHex, _ := hex.DecodeString(string(hexBytes))
	return ed25519.PublicKey(decodedHex)
}

func main() {
	private := GetPrivateKey()
	public := GetPublicKey()

	//-------------------generate key
	pub, pri, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Printf("%x\n", pub)
	fmt.Printf("%x\n", pri)

	// private = ed25519.PrivateKey(pri)
	// public = ed25519.PublicKey(pub)
	//-------------------

	message := []byte("test message")
	sig := ed25519.Sign(private, message)
	if !ed25519.Verify(public, message, sig) {
		fmt.Println("valid signature rejected")
	} else {
		fmt.Println("valid signature pass")
	}
	fmt.Printf("sig: %x\n", sig)

	wrongMessage := []byte("wrong message")
	if ed25519.Verify(public, wrongMessage, sig) {
		fmt.Println("signature of different message accepted")
	} else {
		fmt.Println("wrong signature reject")
	}
}
