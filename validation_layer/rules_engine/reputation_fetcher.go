/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 Fetch latest repuation from the reputation server API
=================================*/

package rules_engine

import (
	"fmt"

	"encoding/json"

	"errors"
	"io/ioutil"
	"net/http"
	"tendermintpoc/reputation_server/types"
	"tendermintpoc/utils"
)

type ReputationFetcher struct {
	url       string
	data      map[string]*types.ReputationItem
	timestamp string
}

// var HasPrint bool = false
var ScoreTotal int = 0 //not right,just for demo print

func NewReputationFetcher(url string) *ReputationFetcher {
	return &ReputationFetcher{url: url, timestamp: utils.GetTimeStamp()}
}

func (f *ReputationFetcher) GetReputationLists() error {
	if f.url == "" {
		return errors.New("wrong url")
	}
	url := f.url
	dataList := types.NewReputationList()

	resp, err := http.Get(url)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return err
	}
	if err = json.Unmarshal(body, &dataList); err != nil {
		return err
	}

	f.data = make(map[string]*types.ReputationItem)

	sum := 0
	for _, v := range dataList {
		sum += v.Reputation
	}

	for _, v := range dataList {
		f.data[v.PublicKey] = v
		if sum != ScoreTotal {
			fmt.Printf("Current reputation : %v\n", v)
		}
	}
	if sum != ScoreTotal {
		ScoreTotal = sum
	}

	// f.data = dataList
	f.timestamp = utils.GetTimeStamp()
	return nil
}
