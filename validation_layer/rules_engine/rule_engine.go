/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :

	RuleEngine is used by Validation Layer to check each bgTx by different rule you decide.

	this can be further decoupled by define `rules`

=================================*/
package rules_engine

import (
	"errors"
	"fmt"
	"strconv"
	"tendermintpoc/blockgraph/types"
	crypto "tendermintpoc/validation_layer/crypto"
	"time"
)

type RuleEngine struct {
	reputationCache *ReputationFetcher
}

func NewRuleEngine(reputationServerUrl string) *RuleEngine {
	re := &RuleEngine{}
	re.reputationCache = NewReputationFetcher(reputationServerUrl)
	re.UpdateReputation()

	go func() {
		for {
			time.Sleep(5 * time.Second)
			re.UpdateReputation()
		}
	}()

	return re
}

func (re *RuleEngine) UpdateReputation() error {
	return re.reputationCache.GetReputationLists()
}

//Validator Logic to check if a bg TX is good
func (re *RuleEngine) CheckBgTransaction(tx *types.BlockgraphTransaction) (bool, error) {
	sb := tx.Submitter

	//The rules

	fmt.Println("check publickey: " + string(sb.Pubkey))
	//[1]check reputation
	if rp, ok := re.reputationCache.data[string(sb.Pubkey)]; ok && rp.Reputation < 30 {
		return false, errors.New("Reputation too low : " + strconv.Itoa(re.reputationCache.data[string(sb.Pubkey)].Reputation))
	}

	//[2]check signature
	if tx.Submitter != nil {
		pub := &crypto.Ed25519PublicKey{PublicKey: crypto.GetPublicKeyEd25519(tx.Submitter.Pubkey)}
		if err := pub.Verify(tx.TxData, tx.Submitter.Signature); err != nil {
			return false, errors.New("Verify Failed for " + string(tx.Submitter.Name) + " " + err.Error())
		}
	} else {
		return false, errors.New("Verify Failed for Nil Submitter")
	}

	for _, p := range tx.TxParticipants {
		pub := &crypto.Ed25519PublicKey{PublicKey: crypto.GetPublicKeyEd25519(p.Pubkey)}
		if err := pub.Verify(tx.TxData, p.Signature); err != nil {
			return false, errors.New("Verify Failed for " + string(p.Name) + " " + err.Error())
		}
	}

	return true, nil
}
