/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 	Pull txs from Kafka and send them to Blockchain

=================================*/

package main

import (
	// "bytes"
	// "encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	// "io/ioutil"
	"net/http"
	"strconv"
	"strings"
	blockchain "tendermintpoc/blockchain"

	transaction "tendermintpoc/blockgraph/types"
	"tendermintpoc/validation_layer/rules_engine"
	txbuffer "tendermintpoc/validation_layer/tx_buffer_service"
	"time"
)

var (
	brokerList    string
	urlBlockchain string
	reputationUrl string
	bgSubmitter   *txbuffer.BlockgraphTransactionSubmitter
)

// var TendermintTxSecondThreshold = 100
var CounterSlice []transaction.TransactionCounter
var PerSecondCounter transaction.TransactionCounter
var TotalCounter transaction.TransactionCounter

const (
	DefaultKafkaBrokerListUrl = "localhost:32979"
	DefaultBlockchainUrl      = "localhost:46667"
	DefaultReputationUrl      = "localhost:9092"
)

type ValidationLayerServer struct {
	ruleEngine       *rules_engine.RuleEngine
	UrlBlockchain    string
	blockchianPlugin blockchain.BlockchainPlugin
}

func NewValidationLayerServer(reputationUrl string) *ValidationLayerServer {
	server := &ValidationLayerServer{}
	server.ruleEngine = rules_engine.NewRuleEngine(reputationUrl)
	server.blockchianPlugin = blockchain.GetBlockchainPlugin()
	return server
}

func blockGraphRequestHandler(w http.ResponseWriter, r *http.Request) {
	// userName := r.URL.Query().Get("name") // get the first value
	// io.WriteString(w, "Hello world!")

	decoder := json.NewDecoder(r.Body)
	var validatoinTx transaction.ValidationTransaction

	err := decoder.Decode(&validatoinTx)
	if err != nil {
		fmt.Printf("Error: fail to parse request, %s\n", err)
		io.WriteString(w, "Error: fail to parse request!")
		return
	}
	defer r.Body.Close()

	bgTx := &transaction.BlockgraphTransaction{
		TxId:           []byte(strconv.Itoa(int(time.Now().Unix() / 1000))),
		TxType:         100,
		TxData:         validatoinTx.Data,
		TxParticipants: validatoinTx.TxParticipants,
		Submitter:      validatoinTx.Submitter,
	}

	// fmt.Printf("Received Request Data is: %s\n", validatoinTx.Data)

	partition, offset, err := bgSubmitter.SubmitBlockgraphTransactionSync(bgTx)
	if err != nil {
		fmt.Printf("Send Error : %s\n", err.Error())
		io.WriteString(w, fmt.Sprintf("Send Error : %s\n", err.Error()))
	}

	TotalCounter.ValidationTxCnt++
	PerSecondCounter.ValidationTxCnt++

	// fmt.Printf("Partition = %d, offset=%d\n", partition, offset)
	io.WriteString(w, fmt.Sprintf("Partition = %d, offset=%d\n", partition, offset))
}

func getCounterRecords(w http.ResponseWriter, r *http.Request) {
	var outStr = "timestamp,validation_transaction_count,tendermint_transaction_count\n"
	for _, record := range CounterSlice {
		outStr += fmt.Sprintf("%d,%d,%d\n", record.Timestamp, record.ValidationTxCnt, record.TendermintTxCnt)
	}

	outStr += fmt.Sprintf("Total Count: %d,%d,%d\n", TotalCounter.Timestamp, TotalCounter.ValidationTxCnt, TotalCounter.TendermintTxCnt)
	io.WriteString(w, outStr)
}

func startSubmitTransactionServer() {
	http.HandleFunc("/send_tx", blockGraphRequestHandler)
	http.HandleFunc("/get_cnt", getCounterRecords)
	http.ListenAndServe(":8088", nil)
}

func counterTicker(interval time.Duration) {
	ticker := time.NewTicker(interval)

	for {
		// fmt.Println("Ticker stat...")
		<-ticker.C

		if PerSecondCounter.ValidationTxCnt > 0 || PerSecondCounter.TendermintTxCnt > 0 {
			PerSecondCounter.Timestamp = time.Now().Unix()
			CounterSlice = append(CounterSlice, PerSecondCounter)
		}

		PerSecondCounter.ValidationTxCnt = 0
		PerSecondCounter.TendermintTxCnt = 0
	}
}

func InitBlockchain(url string) {
	blockchain.InitTendermintPlugin(url)
	blockchain.SetPlugin(blockchain.PLUGIN_TENDERMINT)
}

func main() {
	//FIXME this is a statistic that can be move to Prometheus
	go counterTicker(time.Second)
	TotalCounter.Timestamp = time.Now().Unix()

	flag.StringVar(&reputationUrl, "reputationUrl", DefaultReputationUrl, "borkerlists split by ','")

	flag.StringVar(&brokerList, "brokerList", DefaultKafkaBrokerListUrl, "borkerlists split by ','")
	flag.StringVar(&urlBlockchain, "blockchainUrl", DefaultBlockchainUrl, "blockchain node url")

	flag.Parse()

	InitBlockchain(urlBlockchain)
	server := NewValidationLayerServer(reputationUrl)

	brokers := strings.Split(brokerList, ",")
	if len(brokers) == 0 {
		fmt.Println("invalid borkers list, at least one")
		return
	}

	//start bgSubmitter
	bgSubmitter = txbuffer.NewBlockgraphTransactionSubmitter(brokers, "blockgraph")

	if err := bgSubmitter.StartSyncProducer(); err != nil {
		fmt.Printf("Init Error : %s\n", err.Error())
		return
	}
	defer bgSubmitter.CloseSyncProducer()

	go startSubmitTransactionServer()

	//start bgConsumer
	server.UrlBlockchain = urlBlockchain
	handler := txbuffer.NewBlockgraphTransactionConsumer(brokers, "blockgraph")
	if err := handler.StartConsumer(); err != nil {
		fmt.Printf("Error in StartConsumer: %s", err.Error())
		return
	}

	if output, err := handler.ConsumeTransactions(5); err != nil {
		fmt.Printf("Error in ConsumeTransactions: %s", err.Error())
		return
	} else {
		for tx := range output {
			time.Sleep(100 * time.Millisecond)
			//now get the tx
			// 1: Validate the tx , check its reputation
			// 2: Fill and sign your name in tx.Validator
			// 3: submit to tendermint Core
			// 4: you can start a goroutine to handle this tx
			fmt.Printf("Handle tx content: %s \n", string(tx.TxData))
			ok, err := server.ruleEngine.CheckBgTransaction(tx)
			if err != nil {
				fmt.Printf("%c[1;31;40mCheck Transaction Fail: %s %c[0m \n", 0x1B, err.Error(), 0x1B)
			}

			if ok {
				fmt.Printf("%c[1;32;40mCheck Transaction Pass %c[0m \n", 0x1B, 0x1B)
				//sign your name
				TotalCounter.TendermintTxCnt++
				PerSecondCounter.TendermintTxCnt++
				sendErr := server.blockchianPlugin.GetBlockchainTransactionManager().TransactionSubmit(tx)
				if sendErr != nil {
					fmt.Printf("Submit Transaction Fail: %s \n", sendErr.Error())
				}
			}

		}
	}

}
