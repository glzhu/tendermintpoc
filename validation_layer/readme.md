# About this package
the logic of validation layer.
Two type of validation should be here : 
* validation of identity (by signature)
* validation of business logic 
    * check reputation score
    * use some logic under blockgraph package(not implemented now)


# Notice
in the demo we use:
* /example/example_send_transaction.go as the client
* /server/server.go as the server