package tx_buffer_service

import (
	"fmt"
	"github.com/Shopify/sarama"
	// "log"
	"encoding/json"
	"log"
	"tendermintpoc/blockgraph/types"
	// "time"
	"sync"
	// "fmt"
	"errors"
	"time"
)

type BlockgraphTransactionConsumer struct {
	Brokers []string
	topic   string
	// config            *sarama.Config
	consumer sarama.Consumer
}

//para : borker list , split by ","
func NewBlockgraphTransactionConsumer(brokers []string, topic string) *BlockgraphTransactionConsumer {
	submitter := &BlockgraphTransactionConsumer{Brokers: brokers, topic: topic}
	return submitter
}

func (consumer *BlockgraphTransactionConsumer) StartConsumer() error {
	saramaConsumer, err := sarama.NewConsumer(consumer.Brokers, nil)
	if err != nil {
		return err
	}
	consumer.consumer = saramaConsumer
	return nil
}

func (consumer *BlockgraphTransactionConsumer) ConsumeTransactions(timeRate int) (<-chan *types.BlockgraphTransaction, error) {

	outputChan := make(chan *types.BlockgraphTransaction, 10)
	partitionList, err := consumer.consumer.Partitions(consumer.topic)
	if err != nil {
		return outputChan, err
	}

	fmt.Printf("partition list: %v", partitionList)

	go func() {
		var wg sync.WaitGroup
		for partition := range partitionList {
			pc, err := consumer.consumer.ConsumePartition(consumer.topic, int32(partition), sarama.OffsetNewest)
			if err != nil {
				fmt.Printf("Error in consuming %s\n", err.Error())
				return
			}

			defer pc.AsyncClose()

			wg.Add(1)

			go func(sarama.PartitionConsumer) {
				defer wg.Done()
				for msg := range pc.Messages() {
					time.Sleep(time.Duration(timeRate) * time.Millisecond)
					fmt.Printf("Partition:%d, Offset:%d, Key:%s, Value:%s\n", msg.Partition, msg.Offset, string(msg.Key), string(msg.Value))
					tx := &types.BlockgraphTransaction{}
					if err := json.Unmarshal([]byte(msg.Value), tx); err != nil {
						log.Printf("Unmarshal error %s\n", string(msg.Value))
					} else {
						outputChan <- tx
					}
				}

			}(pc)
		}
		wg.Wait()
	}()

	return outputChan, nil

}

func (consumer *BlockgraphTransactionConsumer) CloseConsumer() error {
	if consumer.consumer != nil {
		consumer.consumer.Close()
		return nil
	} else {
		return errors.New("consumer.consumer is nil")
	}

}
