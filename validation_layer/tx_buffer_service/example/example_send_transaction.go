/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 	This is a client to send Tx to kafka(hardcode topic: blockgraph)

 	single :
 		wait for standard input , you input one line(a string), the client build it as a TX and send.

 	batch :
 		use multi(2 now) goroutine to send totally 1200 txs


=================================*/

package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	ed25519 "golang.org/x/crypto/ed25519"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"
	transaction "tendermintpoc/blockgraph/types"
	crypto "tendermintpoc/validation_layer/crypto"
)

var mode string

func GetPrivateKey() []byte {
	return []byte("6075D2AECA01C5FE0245A4C35F3616F4179010A2FEFC0D7C6BFB4AA4B91D2407B3CB528297BE25E1126AAF3EE58EFDB1C2BBEB3537E5030E70C24E72F6C9D9DD")
}

func GetPublicKey() []byte {
	return []byte("B3CB528297BE25E1126AAF3EE58EFDB1C2BBEB3537E5030E70C24E72F6C9D9DD")
}

func GetPrivateKeyEd25519(key []byte) ed25519.PrivateKey {
	hexBytes := key
	decodedHex, _ := hex.DecodeString(string(hexBytes))
	return ed25519.PrivateKey(decodedHex)
}

func GetPublicKeyEd25519(key []byte) ed25519.PublicKey {
	hexBytes := key
	decodedHex, _ := hex.DecodeString(string(hexBytes))
	return ed25519.PublicKey(decodedHex)
}

func main() {

	flag.StringVar(&mode, "mode", "single", "single or batch")
	flag.Parse()

	var myPub crypto.PublicKey = &crypto.Ed25519PublicKey{PublicKey: GetPublicKeyEd25519(GetPublicKey())}
	var myPri crypto.PrivateKey = &crypto.Ed25519PrivateKey{PrivateKey: GetPrivateKeyEd25519(GetPrivateKey())}

	//Fake pub-pri key for A ,B
	priA := []byte("9a5c9585743cbf3fa358b313f90a4a96c57cb1b0014eb52af4fe0ec9c02dd4976898d99b8d9d1aa61d2d77de9f83039d71bed3d157a5ddc0b643dfcaf1c927d3")
	pubA := []byte("6898d99b8d9d1aa61d2d77de9f83039d71bed3d157a5ddc0b643dfcaf1c927d3")
	var aPubCrypto crypto.PublicKey = &crypto.Ed25519PublicKey{PublicKey: GetPublicKeyEd25519(pubA)}
	var aPriCrypto crypto.PrivateKey = &crypto.Ed25519PrivateKey{PrivateKey: GetPrivateKeyEd25519(priA)}

	priB := []byte("7dd7539a0cb2db523550fa055ee74186516405a49a7fc5292724dd1553846500c71c29d016868e4c3912a8aa3f55e859b79b817e2a51e4bdcfe43dd828714ca0")
	pubB := []byte("c71c29d016868e4c3912a8aa3f55e859b79b817e2a51e4bdcfe43dd828714ca0")
	var bPubCrypto crypto.PublicKey = &crypto.Ed25519PublicKey{PublicKey: GetPublicKeyEd25519(pubB)}
	var bPriCrypto crypto.PrivateKey = &crypto.Ed25519PrivateKey{PrivateKey: GetPrivateKeyEd25519(priB)}

	fmt.Printf("A: %x %x\nB:%x %x\nC:%x %x\n", aPubCrypto, aPriCrypto, bPubCrypto, bPriCrypto, myPub, myPri)

	if mode == "single" {

		fmt.Println("Waiting for your intput...")
		var value string
		for {
			_, err := fmt.Scanf("%s", &value)
			if err != nil {
				break
			}

			signatureA, _ := aPriCrypto.Sign([]byte(value))
			A := &transaction.Participant{
				Pubkey:    pubA,
				Name:      []byte("Participant A"),
				Signature: signatureA,
			}

			signatureB, _ := bPriCrypto.Sign([]byte(value))
			B := &transaction.Participant{
				Pubkey:    pubB,
				Name:      []byte("Participant B"),
				Signature: signatureB,
			}

			//submitter  (it is me)
			signatureC, _ := myPri.Sign([]byte(value))
			C := &transaction.Participant{
				Pubkey:    GetPublicKey(),
				Name:      []byte("Submitter C"),
				Signature: signatureC,
			}

			// value = value + "_new"
			validatoinTx := transaction.ValidationTransaction{
				Data:           []byte(value),
				TxParticipants: []*transaction.Participant{A, B},
				Submitter:      C,
			}

			txStr, err := json.Marshal(validatoinTx)
			if err != nil {
				fmt.Printf("Error: %s\n", err)
			}

			resp, err := http.Post("http://localhost:8088/send_tx", "application/json", bytes.NewBuffer(txStr))
			if err != nil {
				fmt.Printf("Error: %s\n", err)
			}

			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Printf("Error: %s\n", err)
			}
			resp.Body.Close()

			fmt.Printf(string(bodyBytes))
		}
	} else if mode == "batch" {
		var wg sync.WaitGroup
		for i := 1; i <= 2; i++ {
			wg.Add(1)
			sendTh := func() {
				for data := 10000; data < 10600; data++ {
					fmt.Printf("data:%d\n", data)
					value := strconv.Itoa(data)

					signatureA, _ := aPriCrypto.Sign([]byte(value))
					A := &transaction.Participant{
						Pubkey:    pubA,
						Name:      []byte("AAA"),
						Signature: signatureA,
					}

					signatureB, _ := bPriCrypto.Sign([]byte(value))
					B := &transaction.Participant{
						Pubkey:    pubB,
						Name:      []byte("BBB"),
						Signature: signatureB,
					}

					//submitter  (it is me)
					signatureC, _ := myPri.Sign([]byte(value))
					C := &transaction.Participant{
						Pubkey:    GetPublicKey(),
						Name:      []byte("tm_node4"),
						Signature: signatureC,
					}

					validatoinTx := transaction.ValidationTransaction{
						Data:           []byte(value),
						TxParticipants: []*transaction.Participant{A, B},
						Submitter:      C,
					}

					txStr, err := json.Marshal(validatoinTx)
					if err != nil {
						fmt.Printf("Error: %s\n", err)
					}

					runPost := func(txStr []byte) {
						resp, err := http.Post("http://localhost:8088/send_tx", "application/json", bytes.NewBuffer(txStr))
						if err != nil {
							fmt.Printf("Error: %s\n", err)
						}
						bodyBytes, err := ioutil.ReadAll(resp.Body)
						if err != nil {
							fmt.Printf("Error: %s\n", err)
						}
						resp.Body.Close()

						fmt.Printf(string(bodyBytes))

					}

					runPost(txStr)

				}
				wg.Done()
			}
			go sendTh()
		}
		wg.Wait()
	}

}
