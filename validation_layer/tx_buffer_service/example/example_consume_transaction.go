/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 	A example of the validation server how to consume tx from kafka
=================================*/

package main

import (
	"fmt"
	// "strconv"
	txbuffer "tendermintpoc/blockgraph/tx_buffer_service"
	// "tendermintpoc/validation_layer/types"
	// "time"
)

func main() {
	BROKER_URL := "10.124.72.96:32979"

	handler := txbuffer.NewBlockgraphTransactionConsumer([]string{BROKER_URL}, "blockgraph")
	if err := handler.StartConsumer(); err != nil {
		fmt.Printf("Error in StartConsumer: %s", err.Error())
		return
	}

	if output, err := handler.ConsumeTransactions(10); err != nil {
		fmt.Printf("Error in ConsumeTransactions: %s", err.Error())
		return
	} else {
		for tx := range output {
			//now get the tx
			// 1: Validate the tx , check its reputation
			// 2: Fill and sign your name in tx.Validator
			// 3: submit to tendermint Core
			// 4: you can start a goroutine to handle this tx

			fmt.Printf("Handle tx content: %s \n", string(tx.TxData))

		}
	}

}
