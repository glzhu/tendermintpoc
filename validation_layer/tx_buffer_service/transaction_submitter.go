package tx_buffer_service

import (
	"fmt"
	"github.com/Shopify/sarama"
	// "log"
	"encoding/json"
	"errors"
	"tendermintpoc/blockgraph/types"
	"time"
)

type BlockgraphTransactionSubmitter struct {
	Brokers           []string
	topic             string
	config            *sarama.Config
	syncProducer      sarama.SyncProducer
	asyncProducer     sarama.AsyncProducer
	asyncSuccHandler  AsyncSuccHandler
	asyncErrorHandler AsyncErrorHandler
}

type AsyncSuccHandler func(*sarama.ProducerMessage) error
type AsyncErrorHandler func(err *sarama.ProducerError) error

//para : borker list , split by ","
func NewBlockgraphTransactionSubmitter(brokers []string, topic string) *BlockgraphTransactionSubmitter {
	submitter := &BlockgraphTransactionSubmitter{Brokers: brokers, topic: topic}
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Partitioner = sarama.NewRandomPartitioner
	config.Producer.Return.Successes = true
	submitter.config = config

	return submitter
}

func (submitter *BlockgraphTransactionSubmitter) StartSyncProducer() error {
	producer, err := sarama.NewSyncProducer(submitter.Brokers, submitter.config)
	if err != nil {
		return err
	}
	submitter.syncProducer = producer
	return nil
}

func (submitter *BlockgraphTransactionSubmitter) StartAsyncProducer(succHandler AsyncSuccHandler, errHandler AsyncErrorHandler) error {
	submitter.asyncSuccHandler = succHandler
	submitter.asyncErrorHandler = errHandler

	if producer, err := sarama.NewAsyncProducer(submitter.Brokers, submitter.config); err != nil {
		return err
	} else {
		submitter.asyncProducer = producer

		go func(p sarama.AsyncProducer) {
			errors := p.Errors()
			success := p.Successes()
			for {
				select {
				case err := <-errors:
					if err != nil {
						if submitter.asyncErrorHandler != nil {
							submitter.asyncErrorHandler(err)
						}
						fmt.Printf("%s\n", err.Error())
					}
				case msg := <-success:
					//*sarama.ProducerMessage
					if submitter.asyncSuccHandler != nil {
						submitter.asyncSuccHandler(msg)
					}
					fmt.Printf("%s\n", "success return")
				}
			}
		}(producer)

	}
	return nil
}

func (submitter *BlockgraphTransactionSubmitter) SubmitBlockgraphTransactionAsync(tx *types.BlockgraphTransaction) error {

	if submitter.asyncProducer == nil {
		return errors.New("asyncProducer is nil , need StartAsyncProducer first ")
	}

	msg := &sarama.ProducerMessage{
		Topic:     submitter.topic,
		Partition: int32(-1),
		Key:       sarama.StringEncoder("key" + time.Now().String()),
	}

	if jsonValue, err := json.Marshal(tx); err != nil {
		return err
	} else {
		fmt.Println(string(jsonValue))
		msg.Value = sarama.ByteEncoder(jsonValue)
	}

	submitter.asyncProducer.Input() <- msg
	return nil
}

func (submitter *BlockgraphTransactionSubmitter) SubmitBlockgraphTransactionSync(tx *types.BlockgraphTransaction) (int32, int64, error) {
	if submitter.syncProducer == nil {
		return -1, -1, errors.New("syncProducer is nil , need StartSyncProducer first ")
	}

	msg := &sarama.ProducerMessage{
		Topic:     submitter.topic,
		Partition: int32(-1),
		Key:       sarama.StringEncoder("key" + time.Now().String()),
	}

	if jsonValue, err := json.Marshal(tx); err != nil {
		return -1, -1, err
	} else {
		// fmt.Println(string(jsonValue))
		msg.Value = sarama.ByteEncoder(jsonValue)
	}

	partition, offset, err := submitter.syncProducer.SendMessage(msg)
	if err != nil {
		return -1, -1, err
	}
	// fmt.Printf("Partition = %d, offset=%d\n", partition, offset)

	return partition, offset, nil
}

func (submitter *BlockgraphTransactionSubmitter) CloseSyncProducer() error {
	if submitter.syncProducer != nil {
		submitter.syncProducer.Close()
		return nil
	} else {
		return errors.New("submitter.syncProducer is nil")
	}

}

func (submitter *BlockgraphTransactionSubmitter) CloseAsyncProducer() error {
	if submitter.asyncProducer != nil {
		submitter.asyncProducer.Close()
		return nil
	} else {
		return errors.New("submitter.asyncProducer is nil")
	}
}

// func main() {

// 	producer, err := sarama.NewAsyncProducer([]string{BROKER_URL}, config)
// 	if err != nil {
// 		panic(err)
// 	}

// 	defer producer.Close()

// 	go func(p sarama.AsyncProducer) {
// 		errors := p.Errors()
// 		success := p.Successes()
// 		for {
// 			select {
// 			case err := <-errors:
// 				if err != nil {
// 					fmt.Printf("%s\n", err.Error())
// 				}
// 			case <-success:
// 				fmt.Printf("%s\n", "success return")
// 			}
// 		}
// 	}(producer)

// 	msg := &sarama.ProducerMessage{
// 		Topic:     "blockgraph",
// 		Partition: int32(-1),
// 		Key:       sarama.StringEncoder("key" + time.Now().String()),
// 	}

// 	var value string
// 	for {
// 		_, err := fmt.Scanf("%s", &value)
// 		if err != nil {
// 			break
// 		}
// 		msg.Value = sarama.ByteEncoder(value)
// 		fmt.Println(value)

// 		producer.Input() <- msg
// 		// partition, offset, err := producer.SendMessage(msg)
// 		// if err != nil {
// 		// 	fmt.Println("Send message Fail")
// 		// }
// 		// fmt.Printf("Partition = %d, offset=%d\n", partition, offset)
// 	}
// }
