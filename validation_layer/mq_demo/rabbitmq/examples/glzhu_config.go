package main

const (
	SERVER_DSN    = "amqp://bg:bg@localhost:5672/bg"
	QUEUE_NAME    = "bg_queue"
	EXCHANGE_NAME = "bg_exchange"
	ROUTE_KEY     = "bgr"
)
