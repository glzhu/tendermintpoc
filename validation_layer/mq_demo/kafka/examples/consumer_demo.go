package main

import (
	"fmt"
	"sync"

	"github.com/Shopify/sarama"
)

const (
	BROKER_URL = "10.124.72.96:32979"
)

var (
	wg sync.WaitGroup
)

func main() {
	consumer, err := sarama.NewConsumer([]string{BROKER_URL}, nil)
	if err != nil {
		panic(err)
	}

	partitionList, err := consumer.Partitions("blockgraph")
	if err != nil {
		panic(err)
	}

	fmt.Printf("partition list: %v", partitionList)

	for partition := range partitionList {
		pc, err := consumer.ConsumePartition("blockgraph", int32(partition), sarama.OffsetNewest)
		if err != nil {
			panic(err)
		}

		defer pc.AsyncClose()

		wg.Add(1)

		go func(sarama.PartitionConsumer) {
			defer wg.Done()
			for msg := range pc.Messages() {
				fmt.Printf("Partition:%d, Offset:%d, Key:%s, Value:%s\n", msg.Partition, msg.Offset, string(msg.Key), string(msg.Value))
			}

		}(pc)
	}
	wg.Wait()
	consumer.Close()
}
