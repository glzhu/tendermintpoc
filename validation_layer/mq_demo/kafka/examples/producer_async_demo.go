package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	// "log"
	"time"
)

const (
	BROKER_URL = "10.124.72.96:32980"
)

func main() {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Partitioner = sarama.NewRandomPartitioner
	config.Producer.Return.Successes = true

	producer, err := sarama.NewAsyncProducer([]string{BROKER_URL}, config)
	if err != nil {
		panic(err)
	}
	defer producer.Close()

	go func(p sarama.AsyncProducer) {
		errors := p.Errors()
		success := p.Successes()
		for {
			select {
			case err := <-errors:
				if err != nil {
					fmt.Printf("%s\n", err.Error())
				}
			case <-success:
				fmt.Printf("%s\n", "success return")
			}
		}
	}(producer)

	msg := &sarama.ProducerMessage{
		Topic:     "blockgraph",
		Partition: int32(-1),
		Key:       sarama.StringEncoder("key" + time.Now().String()),
	}

	var value string
	for {
		_, err := fmt.Scanf("%s", &value)
		if err != nil {
			break
		}
		msg.Value = sarama.ByteEncoder(value)
		fmt.Println(value)

		producer.Input() <- msg
		// partition, offset, err := producer.SendMessage(msg)
		// if err != nil {
		// 	fmt.Println("Send message Fail")
		// }
		// fmt.Printf("Partition = %d, offset=%d\n", partition, offset)
	}
}
