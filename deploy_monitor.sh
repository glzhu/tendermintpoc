
# This script help us to start and shutdown monitor service in docker-compose

usage() {
cat << EOF
Usage: $(basename $0) command [options]

command:
  help      show this help message
  clean     clean the monitor cluster
  start     start the monitor cluster
  

Options:
  -h|--help show this help
EOF
exit 0
}


main() {
    case $1 in
        help|-h|--help) usage ;;
        c|clean ) clean ;;
        s|start) start ;;
    esac
}

clean() {
	cd ./monitor_service/monitor_deployment_docker/ && docker-compose down
}

start() {
	docker network create blockgraph 
	cd ./monitor_service/monitor_deployment_docker/ && docker-compose up -d
}

main $@