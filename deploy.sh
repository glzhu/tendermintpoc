#!/bin/bash
# Author            : Hongbo Liu <hbliu@freewheel.tv>
# Date              : 2018-05-02
# Last Modified Date: 2018-05-02
# Last Modified By  : Hongbo Liu <hbliu@freewheel.tv>


usage() {
cat << EOF
Usage: $(basename $0) command [options]

command:
  help      show this help message
  clean     clean the kafka and tendermint cluster
  start     start the tendermint and kafka cluster
  server    validation layer server which will validate request and send to tendermint cluster once validate passed
  client    example client which interact with the server

Options:
  -h|--help show this help
EOF
exit 0
}

kafka_dir="./validation_layer/mq_demo/kafka/docker-kafka"
tendermint_dir="./tendermint-deploy/docker-compose-local"

getip() {
    IP=$(ifconfig en0 | grep 'inet ' | cut -d ' ' -f 2)

    echo $IP
}

update_kafka_compose_file() {
    if ! which yq &> /dev/zero; then
        go get github.com/mikefarah/yq
    fi

    yq write -i $kafka_dir/docker-compose.yml services.kafka.environment.KAFKA_ADVERTISED_HOST_NAME $(getip)
}

clean() {
    (cd $kafka_dir && docker-compose down &> /dev/zero)
    (cd $tendermint_dir && docker-compose down &> /dev/zero)
}

get_kafka_port() {
    echo $(docker ps --format '{{ .Ports }},{{ .Names }}' | grep kafka_1 | cut -d'-' -f1 | cut -d':' -f2)
}

start_cluster() {
    docker network create blockgraph 
    make -C $tendermint_dir &
    update_kafka_compose_file
    (cd $kafka_dir && docker-compose up -d && docker-compose scale kafka=3)
}

start_server() {
    server_dir="./validation_layer/server"
    cd $server_dir
    rm server
    go get
    go build
    cd -
    $server_dir/server --reputationUrl=http://localhost:9092/reputations  --blockchainUrl=http://localhost:46667  --brokerList=localhost:$(get_kafka_port)
        # &> log/server.log &
}

init() {
    mkdir -p log
}

client() {
    broker_url="$(getip):$(get_kafka_port)"
    BG_BROKER_URL=$broker_url go run validation_layer/tx_buffer_service/example/example_send_transaction.go
}

send_batch(){
    broker_url="$(getip):$(get_kafka_port)"
    BG_BROKER_URL=$broker_url go run validation_layer/tx_buffer_service/example/example_send_transaction.go --mode=batch    
}

main() {
    init
    case $1 in
        help|-h|--help) usage ;;
        c|clean ) clean ;;
        s|start) start_cluster ;;
        server) start_server ;;
        client) client ;;
        send_batch) send_batch ;;
    esac
}

main $@
