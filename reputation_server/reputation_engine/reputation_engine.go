/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 ReputationEngine
 	InitReputationEngine :  init
 	AddNode  :  add a node with an initial tech repuatation
	RemoveNode  : remove a node in the tech repuatation map
	GetTechReputation : return the reputation map
	UpdateTechReputation :  it is the calculation engine.  (can be decoupled by defining `rules`)

	FetchXXXx : use the monitorReader to get monitor metrics

=================================*/

package reputation_engine

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"tendermintpoc/monitor_service/prometheus_reader"
)

type ReputationEngine struct {
	monitorReader  *prometheus_reader.PrometheusReader
	techReputation map[string]int64
	UpdateSpan     int
}

func (this *ReputationEngine) InitReputationEngine(keyList []string, valueList []int64, prometheusUrl string) error {
	this.techReputation = make(map[string]int64, 10)

	if len(keyList) != len(valueList) {
		return errors.New("length of key,value list do not match")
	}

	for i := 0; i < len(keyList); i++ {
		this.techReputation[keyList[i]] = valueList[i]
	}

	this.monitorReader = prometheus_reader.NewPrometheusReader(prometheusUrl)
	this.UpdateSpan = 5

	return nil
}

//if already have ,do not init again
func (this *ReputationEngine) AddNode(name string, initValue int64) {
	if _, ok := this.techReputation[name]; !ok {
		this.techReputation[name] = initValue
	}
}

func (this *ReputationEngine) RemoveNode(name string) {
	delete(this.techReputation, name)
}

func (this *ReputationEngine) GetTechReputation() map[string]int64 {
	return this.techReputation
}

func (this *ReputationEngine) UpdateTechReputation() error {
	//check alive
	{
		retMap, err := this.FetchAliveNodes()
		if err != nil {
			return err
		}
		for k, v := range this.techReputation {
			if _, ok := retMap[k]; !ok {
				if v > 0 {
					this.techReputation[k] -= 15
				}
			} else {
				if v < 80 {
					v += 5
				}
			}
		}
	}

	//check CPU
	{
		retMap, err := this.FetchCpuUsage()
		if err != nil {
			return err
		}
		for k, v := range this.techReputation {
			if u, ok := retMap[k]; !ok {
				fmt.Println("miss key in return map :", k)
			} else {
				if u > 20 {
					this.techReputation[k] -= 3
				} else if u > 10 {
					this.techReputation[k] -= 1
				} else if u < 3 && v < 90 {
					this.techReputation[k] += 1
				}
			}
		}
	}

	//check mem
	{
		retMap, err := this.FetchMemUsage()
		if err != nil {
			return err
		}
		for k, v := range this.techReputation {
			if u, ok := retMap[k]; !ok {
				fmt.Println("miss key in return map :", k)
			} else {
				if u > 800 {
					this.techReputation[k] -= 5
				} else if u > 400 {
					this.techReputation[k] -= 3
				} else if u < 200 && v < 90 {
					this.techReputation[k] += 1
				}
			}
		}
	}

	//check disk
	{
		retMap, err := this.FetchDiskUsage()
		if err != nil {
			return err
		}
		for k, v := range this.techReputation {
			if u, ok := retMap[k]; !ok {
				fmt.Println("miss key in return map :", k)
			} else {
				if u > 0.7 {
					this.techReputation[k] -= 10
				} else if u > 0.6 {
					this.techReputation[k] -= 4
				} else if u < 0.5 && v < 90 {
					this.techReputation[k] += 1
				}
			}
		}
	}

	//Random adjustment for Demo
	{
		for k, v := range this.techReputation {
			if v > 95 {
				deduce := rand.Intn(5)
				this.techReputation[k] -= int64(deduce)
				continue
			}

			if v > 70 {
				increase := rand.Intn(5)
				this.techReputation[k] += int64(increase)
				continue
			}

			delta := rand.Intn(6) - 4
			this.techReputation[k] += int64(delta)
		}
	}

	//format to 0
	{
		for k, v := range this.techReputation {
			if v < 0 {
				this.techReputation[k] = 0
			}
		}
	}

	return nil
}

func (this *ReputationEngine) FetchAliveNodes() (map[string]int, error) {
	aliveNodes := make(map[string]int)

	retMap, err := this.monitorReader.QueryNameAndValue("container_last_seen")
	if err != nil {
		return aliveNodes, err
	}
	// fmt.Printf("retmap : %v", retMap)

	for k, _ := range retMap {
		aliveNodes[k] = 1
	}
	return aliveNodes, nil
}

//return 0%-100%
func (this *ReputationEngine) FetchCpuUsage() (map[string]float64, error) {
	ret, err := this.FetchPerformanceUsage("sort_desc(sum(rate(container_cpu_user_seconds_total[30s]))by(name))")
	if err == nil {
		for k, v := range ret {
			ret[k] = v * 100
		}
	}
	return ret, err
}

func (this *ReputationEngine) FetchMemUsage() (map[string]float64, error) {
	ret, err := this.FetchPerformanceUsage("sum(container_memory_usage_bytes)by(name)")
	if err == nil {
		for k, v := range ret {
			ret[k] = v / 1024 / 1024
		}
	}
	return ret, err
}

func (this *ReputationEngine) FetchDiskUsage() (map[string]float64, error) {
	ret, err := this.FetchPerformanceUsage("container_fs_usage_bytes/container_fs_limit_bytes")
	if err == nil {
		for k, v := range ret {
			ret[k] = v * 100 // to percentage
		}
	}
	return ret, err
}

func (this *ReputationEngine) FetchPerformanceUsage(query string) (map[string]float64, error) {
	usageMap := make(map[string]float64)

	retMap, err := this.monitorReader.QueryNameAndValue(query)
	if err != nil {
		return usageMap, err
	}
	// fmt.Printf("retmap : %v", retMap)

	for k, v := range retMap {
		if vst, ok := v.(string); ok {
			f, err := strconv.ParseFloat(vst, 64)
			if err != nil {
				return usageMap, err
			}
			// f = f * 100 //change to percentage
			usageMap[k] = float64(f)
		} else {
			fmt.Println("Error in type assert")
		}
	}
	return usageMap, nil
}
