package reputation_engine

import (
	"fmt"
	"tendermintpoc/monitor_service/prometheus_reader"
	"testing"
)

func TestReputationEngine_FetchAliveNodes(t *testing.T) {
	type fields struct {
		monitorReader  *prometheus_reader.PrometheusReader
		techReputation map[string]int64
		UpdateSpan     int
	}
	tests := []struct {
		name    string
		fields  fields
		want    map[string]int
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "test1",
			fields: fields{
				monitorReader: prometheus_reader.NewPrometheusReader("http://localhost:9090"),
				UpdateSpan:    5,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &ReputationEngine{
				monitorReader:  tt.fields.monitorReader,
				techReputation: tt.fields.techReputation,
				UpdateSpan:     tt.fields.UpdateSpan,
			}
			got, err := this.FetchAliveNodes()
			if (err != nil) != tt.wantErr {
				t.Errorf("ReputationEngine.FetchAliveNodes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for k, _ := range got {
				fmt.Println(k)
			}

			//your nodes name
			node := "tm_node1"
			if _, prs := got[node]; !prs {
				t.Errorf("ReputationEngine.FetchAliveNodes() Error in not have tm_node1 ")
				return
			}

		})
	}
}

func TestReputationEngine_FetchCpuUsage(t *testing.T) {
	type fields struct {
		monitorReader  *prometheus_reader.PrometheusReader
		techReputation map[string]int64
		UpdateSpan     int
	}
	tests := []struct {
		name    string
		fields  fields
		want    map[string]float64
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "test1",
			fields: fields{
				monitorReader: prometheus_reader.NewPrometheusReader("http://localhost:9090"),
				UpdateSpan:    5,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &ReputationEngine{
				monitorReader:  tt.fields.monitorReader,
				techReputation: tt.fields.techReputation,
				UpdateSpan:     tt.fields.UpdateSpan,
			}
			got, err := this.FetchCpuUsage()
			if (err != nil) != tt.wantErr {
				t.Errorf("ReputationEngine.FetchCpuUsage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for k, v := range got {
				fmt.Println(k, ":", v)
			}
		})
	}
}

func TestReputationEngine_UpdateTechReputation(t *testing.T) {
	engine := &ReputationEngine{}
	engine.InitReputationEngine([]string{"tm_node1", "tm_node2", "tm_node3", "tm_node4"},
		[]int64{100, 100, 100, 100}, "http://localhost:9090")

	err := engine.UpdateTechReputation()
	if err != nil {
		t.Errorf("Error %s", err.Error())
	}

	for k, v := range engine.techReputation {
		fmt.Printf("key:%s,rep:%d", k, v)
	}

}
