/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
	define the reputation item

	in blockchain, the nodes may be identified by PublicKey
	in monitor service, now we can only get the nodes by its name, so they are identified by Name

=================================*/

package types

type ReputationItem struct {
	PublicKey            string `json: publickey`
	Reputation           int    `json:reputation`
	AccumulateReputation int    `json:accumulatereputation`
	TechReputation       int    `json:techreputation`
	Name                 string `json: name`
}

type ReputationList []*ReputationItem

func NewReputationList() ReputationList {
	return make([]*ReputationItem, 5)
}
