/*=================================
Author: glzhu
Generate Date: 2018-05-01
Inc: Freewheel
Description :
 A single server to submmit newest reputation to blockchain
 too many hard codes here, should be refactor
=================================*/

package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	blockchain "tendermintpoc/blockchain"
	"tendermintpoc/reputation_server/types"
	"time"
)

var sourceList string
var sourceOne string
var keysDataPath string

var dataList types.ReputationList
var period int

const (
	DefaultReputationListUrl     = "http://reputation:9092/reputations"
	DefaultReputationFetchOneUrl = "http://reputation:9092/reputation"
	DefaultReputationSubmitUrl   = "http://tm_node1:46657"
	DefaultValidatorPubKeysFile  = "./PubKeyList.txt"
)

func getReputationLists(url string) (types.ReputationList, error) {
	dataList = types.NewReputationList()

	resp, err := http.Get(url)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return dataList, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return dataList, err
	}
	if err = json.Unmarshal(body, &dataList); err != nil {
		return dataList, err
	}

	fmt.Printf("res: %v", dataList)
	return dataList, nil
}

func getReputationItem(url string, key string) (*types.ReputationItem, error) {
	list := []*types.ReputationItem{}
	item := &types.ReputationItem{}

	resp, err := http.Get(url + "/" + key)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return item, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return item, err
	}
	fmt.Println(string(body))
	if err = json.Unmarshal(body, &list); err != nil {
		return item, err
	}
	fmt.Printf("res: %v", list[0])
	return list[0], nil
}

func setReputationItemToReputationServer(url string, item *types.ReputationItem) error {
	jsonStr, err0 := json.Marshal(item)
	if err0 != nil {
		return err0
	}
	fmt.Printf("%v", string(jsonStr))

	req, err := http.NewRequest("POST", url+"/"+item.PublicKey, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	return nil
}

func submitPowerChangeTxToTendermint(url string, key string, power int) error {
	fullurl := url + "\"val:01" + key + "/" + strconv.Itoa(power) + "\""
	fmt.Println(fullurl)
	resp, err := http.Get(fullurl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(string(body))

	return nil
}

var validatorList types.ReputationList

func getPubkeyListFromFile(path string) error {

	fi, err := os.Open(path)
	if err != nil {
		return err
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	for {
		line, _, c := br.ReadLine()
		if c == io.EOF {
			break
		}
		fmt.Println(string(line))
		values := strings.Split(string(line), ",")
		num, err := strconv.Atoi(values[2])
		if err != nil {
			fmt.Println(err.Error())
		}
		validatorList = append(validatorList, &types.ReputationItem{PublicKey: values[0], Name: values[1], Reputation: num})
	}
	return nil
}

var blockchianPlugin blockchain.BlockchainPlugin

func InitBlockchain(url string) {
	blockchain.InitTendermintPlugin(url)
	// blockchain.InitQuorumPlugin(nodeAddress)

	blockchain.SetPlugin(blockchain.PLUGIN_TENDERMINT)

	blockchianPlugin = blockchain.GetBlockchainPlugin()
}

func main() {

	flag.StringVar(&sourceList, "sourcelist", DefaultReputationListUrl, "url to get all reputation")
	flag.StringVar(&sourceOne, "sourceOne", DefaultReputationFetchOneUrl, "url to get all reputation")
	flag.StringVar(&keysDataPath, "dataPath", DefaultValidatorPubKeysFile, "dataFilePath to get all public keys")
	flag.IntVar(&period, "period", 30, "update period")

	flag.Parse()

	InitBlockchain(DefaultReputationSubmitUrl)

	dataList = types.NewReputationList()
	validatorList = []*types.ReputationItem{}
	getPubkeyListFromFile(keysDataPath)

	//init setting to repuation server
	for _, v := range validatorList {
		fmt.Printf("Init:  %v \n", v)

		if err := setReputationItemToReputationServer(sourceOne, v); err != nil {
			fmt.Println(err.Error())
		}
	}

	for {
		time.Sleep(time.Duration(period) * time.Second)
		for _, v := range validatorList {
			item, err := getReputationItem(sourceOne, v.PublicKey)
			if err != nil {
				fmt.Println(err.Error())
			}
			err2 := blockchianPlugin.GetBlockchainNodeManager().NodeSetVotingPower(v.PublicKey, int64(item.Reputation))
			if err2 != nil {
				fmt.Println(err2.Error())
			}
		}
	}

}
