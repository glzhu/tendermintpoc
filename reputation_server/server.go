package main

import (
	log "github.com/sirupsen/logrus"
	// "golang.org/x/net/http2"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
	"tendermintpoc/reputation_server/reputation_engine"
	"tendermintpoc/reputation_server/types"
	"time"

	"flag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var ReputationMap map[string]*types.ReputationItem

func init() {
	resetReputation()
}

func resetReputation() {
	ReputationMap = make(map[string]*types.ReputationItem)

}

func mockRepuationItem(name string, key string, power int) *types.ReputationItem {
	return &types.ReputationItem{
		PublicKey:  key,
		Reputation: power,
		Name:       name,
	}
}

func PingHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("get ping")
	fmt.Fprintf(w, "%s", "pong")
}

func GetReputationByIdHandler(w http.ResponseWriter, r *http.Request) {
	var list []*types.ReputationItem
	vars := mux.Vars(r)
	key := vars["pubkey"]

	if item, ok := ReputationMap[key]; ok {
		list = append(list, item)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if js, errMatshal := json.Marshal(list); errMatshal == nil {
		fmt.Fprintf(w, "%v", string(js))
	} else {
		log.Error(errMatshal)
		fmt.Fprintf(w, "%v", errMatshal)
	}
}

func SetReputationByIdHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["pubkey"]

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	item := &types.ReputationItem{}
	if err := json.Unmarshal(body, item); err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	item.PublicKey = key
	item.AccumulateReputation = item.Reputation
	ReputationMap[key] = item

	fmt.Fprintf(w, "ok!")
}

func GetReputationByIdRandomHandler(w http.ResponseWriter, r *http.Request) {
	var list []*types.ReputationItem
	vars := mux.Vars(r)
	key := vars["pubkey"]

	if item, ok := ReputationMap[key]; ok {
		list = append(list, item)
	} else {
		sum := sha256.Sum256([]byte(key + strconv.Itoa(time.Now().Hour())))
		log.Info("generate sum " + fmt.Sprintf("%x", sum))
		v := sum[len(sum)-1] % 100
		log.Info("generate val " + fmt.Sprintf("%d", v))
		list = append(list, &types.ReputationItem{PublicKey: key, Reputation: int(v)})
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if js, errMatshal := json.Marshal(list); errMatshal == nil {
		fmt.Fprintf(w, "%v", string(js))
	} else {
		log.Error(errMatshal)
		fmt.Fprintf(w, "%v", errMatshal)
	}
}

func GetReputationsHandler(w http.ResponseWriter, r *http.Request) {
	var list []*types.ReputationItem

	for _, v := range ReputationMap {
		list = append(list, v)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if js, errMatshal := json.Marshal(list); errMatshal == nil {
		fmt.Fprintf(w, "%v", string(js))
	} else {
		log.Error(errMatshal)
		fmt.Fprintf(w, "%v", errMatshal)
	}
}

//every 10s refresh the reputation (calculation and set back to prometheus)
func RunTechReputationEngine(url string) chan<- int {

	prometheus.MustRegister(reputationGauge)
	prometheus.MustRegister(techReputationGauge)

	engine := &reputation_engine.ReputationEngine{}

	//FIXME : hard code here, these names can be fetched by name service
	engine.InitReputationEngine([]string{"tm_node1", "tm_node2", "tm_node3", "tm_node4"},
		[]int64{100, 100, 100, 100}, url)

	end := make(chan int)
	go func(ch chan int) {
		for {
			select {
			case <-ch:
				break
			default:
				{
					for _, v := range ReputationMap {
						engine.AddNode(v.Name, 100) //now we just set the initial reputation score to 100

						reputationGauge.WithLabelValues(v.Name).Set(float64(v.Reputation))
						techReputationGauge.WithLabelValues(v.Name).Set(float64(v.TechReputation))
					}

					err := engine.UpdateTechReputation()
					if err != nil {
						fmt.Printf("Error %s", err.Error())
					} else {

						techReputation := engine.GetTechReputation()
						for k, v := range techReputation {
							fmt.Printf("key:%s,rep:%d\n", k, v)
						}

						for k, v := range ReputationMap {
							fmt.Printf("%v %v\n", k, v)
							if vv, ok := techReputation[v.Name]; ok {
								ReputationMap[k].TechReputation = int(vv)
								ReputationMap[k].Reputation = int(float64(v.AccumulateReputation)*0.7 + float64(vv)*0.3)
							}
						}

						fmt.Printf("%v\n", ReputationMap)
					}

					time.Sleep(10 * time.Second) //refresh time is 10 sec now
				}
			}
		}

	}(end)
	return end
}

var prometheusUrl string

//Register the prometheus metrics, then the reputation will be published back to prometheus
var (
	reputationGauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "blockgraph",
			Subsystem: "reputation",
			Name:      "node_reputation_score",
			Help:      "Current reputation_score of the node, partitioned by node name.",
		},
		[]string{ // set the tag of the metrics
			// Which node
			"node"},
	)

	techReputationGauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "blockgraph",
			Subsystem: "reputation",
			Name:      "node_tech_reputation_score",
			Help:      "Current tech reputation_score of the node, partitioned by node name.",
		},
		[]string{
			// Which node
			"node"},
	)
)

func main() {
	flag.StringVar(&prometheusUrl, "prometheusUrl", "http://prometheus:9090", "url to connect to monitor")
	flag.Parse()

	endCh := RunTechReputationEngine(prometheusUrl)

	r := mux.NewRouter()
	r.HandleFunc("/ping", PingHandler).Methods("GET")
	r.HandleFunc("/reputation/{pubkey:[0-9a-zA-Z]+}", GetReputationByIdHandler).Methods("GET")
	r.HandleFunc("/reputation/{pubkey:[0-9a-zA-Z]+}", SetReputationByIdHandler).Methods("POST")

	// this is a fake api, to let every key has a random  reputations
	r.HandleFunc("/reputation_ran/{pubkey:[0-9a-zA-Z]+}", GetReputationByIdRandomHandler).Methods("GET")
	r.HandleFunc("/reputations", GetReputationsHandler).Methods("GET")

	r.Handle("/metrics", promhttp.Handler())

	srv := &http.Server{
		Handler: r,
		Addr:    ":9092",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	fmt.Println("Startng server ...")
	log.Fatal(srv.ListenAndServe())
	endCh <- 1 //stop the technical update
	close(endCh)
}
