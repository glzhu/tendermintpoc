# Validation Layer Demo Setup

The whole environment could be setup via [this wiki](https://blockgraph.atlassian.net/wiki/spaces/BLOC/pages/27951286/Sprint1+Tendermint+Reputation+Service+Demo#Sprint1TendermintReputationServiceDemo-1Startupclusterswith4Tendermintnodesand1ReputationService).

We also implemented a script which help you do the most work. You just need to do following step to start the demo:

1. Get the code via `cd $GOPATH/src && git clone git@bitbucket.org:glzhu/tendermintpoc.git`, then `cd tendermintpoc`
1. Start the tendermint and kafka cluster with `./deploy.sh start`
2. Start the validation server with `./deploy.sh server`
3. Start the example client with `./deploy.sh client`